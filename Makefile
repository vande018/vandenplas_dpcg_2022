.PHONY: all newall simulation newdata newsimulation analysis result clean

all: analysis result

newall: newsimulation newdata analysis result

simulation:
	$(MAKE) -f Makefile paper --directory=simulation

newsimulation:
	$(MAKE) -f Makefile new --directory=simulation

newdata: newsimulation
	$(MAKE) -f Makefile all --directory=data

analysis:
	$(MAKE) -f Makefile compute --directory=analysis

result: analysis
	$(MAKE) -f Makefile results --directory=analysis

clean:
	$(MAKE) -f Makefile clean --directory=simulation
	$(MAKE) -f Makefile clean --directory=data
	$(MAKE) -f Makefile clean --directory=analysis
