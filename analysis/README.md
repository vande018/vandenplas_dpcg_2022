# Julia prototype for ssSNPBLUP

## Introduction
This directory contains Julia scripts to run ssSNPBLUP.
The aim of these scripts is 1) to get exact solutions and 2) to test easily some options. Therefore, these scripts are not optimized (but proposed optimizations are welcome).

## Parameter file
A parameter file, e.g. called *param*, must be provided to the main script *main.jl*.
This parameter file must contain the name of:


 * the effect file,
 * the phenotype file (one column with the phenotypes);
 * the pedigree file (3 columns: animal-sire-dam; Animals must be sorted from old to young, and coded from 1 to n);
 * the IDs of the genotyped animals;
 * the SNP genotypes;
 * the residual variance;
 * the additive genetic varianc;
 * the column number(s) of the fixed effect(s) in the effect file;
 * the column number of the animal effect in the effect file;
 * the proportion of residual polygenic variance.


All effects must be numbered consecutively from 1 to *n*.
Currently, it supports only univariate models, but extensions to more complex models should be possible and easy to implement.


## Main script

To run the main script *main.jl*:

```shell
julia main.jl > out.main
```

## Julia modules associated with the main script

The following Julia modules must be associated with the main script (the directory that contain them must be mentioned in the first line of *main.jl*):


 * modblup.jl, modgeneral.jl, modpedigree.jl, modpreconditioner.jl, modsolver.jl


