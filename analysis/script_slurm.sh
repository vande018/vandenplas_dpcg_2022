#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=result.txt
#-----------------------------Other information------------------------
#SBATCH --comment='Delft Thesis'

#-----------------------------Required resources-----------------------
#SBATCH --time=1-00:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --mem-per-cpu=8000

#-----------------------------Environment, Operations and Job steps----

module load julia/1.2.0

julia main.jl >out.main
