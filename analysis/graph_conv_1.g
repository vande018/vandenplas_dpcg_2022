set term pngcairo size 1000,750 font 'Arial, 18'

set ylabel 'Termination criterion (logarithm scale)'
set xlabel 'Iteration'

set key on outside center bottom horizontal

set grid ytics

set xrange [0:1900]
set yrange [-7:1]

#list=system('ls -1B conv*.csv')
set datafile separator ','
#set logscale y 10

set out 'liu_pcg.png'
LIST = 'conviterliu.csv conviterliuold.csv conviterliub.csv conviterliubold.csv'
LABEL = "'PCG - 0' 'PCG - 0.25%' 'B-PCG - 0' 'B-PCG - 0.25%'"
plot for [i=1:words(LIST)] word(LIST,i) u ($1 <= 0 ? NaN : $1):(log(sqrt($3))/log(10)) w l t word(LABEL,i)

set out 'liu_dpcg_sub_100.png'
LIST = 'conviterliud100.csv conviterliud100old.csv conviterliudk100.csv conviterliudk100old.csv'
LABEL = "'Rand. - 0' 'Rand. - 0.25%' 'Kmeans - 0' 'Kmeans - 0.25%'"
plot for [i=1:words(LIST)] word(LIST,i) u ($1 <= 0 ? NaN : $1):(log(sqrt($3))/log(10)) w l t word(LABEL,i)

set out 'liu_dpcg_sub_10.png'
LIST = 'conviterliud10.csv conviterliud10old.csv conviterliudk10.csv conviterliudk10old.csv'
LABEL = "'Rand. - 0' 'Rand. - 0.25%' 'Kmeans - 0' 'Kmeans - 0.25%'"
plot for [i=1:words(LIST)] word(LIST,i) u ($1 <= 0 ? NaN : $1):(log(sqrt($3))/log(10)) w l t word(LABEL,i)


set out 'liu_dpcg_snap_pod.png'
LIST = 'conviterliudsnap.csv conviterliudsnapold.csv  conviterliudpod.csv conviterliudpodold.csv conviterliudpodk10old.csv conviterliudpodk100old.csv'
LABEL =  "'Snap. - 0' 'Snap. - 0.25%' 'POD - 0' 'POD - 0.25%' 'POD + Kmeans 1246 - 0.25%' 'POD + Kmeans 126 - 0.25%'"
plot for [i=1:words(LIST)] word(LIST,i) u ($1 <= 0 ? NaN : $1):(log(sqrt($3))/log(10)) w l t word(LABEL,i)






#set out 'liu_100.png'
#list='conviterliu.csv conviterliud100.csv conviterliud100old.csv conviterliudk100.csv conviterliudk100old.csv conviterliudpodk100.csv conviterliudpodk100old.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
# 
#set out 'liu_10.png'
#list='conviterliu.csv conviterliud10.csv conviterliud10old.csv conviterliudk10.csv conviterliudk10old.csv conviterliudpodk10.csv conviterliudpodk10old.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
#
#set out 'liu_pod.png'
#list='conviterliu.csv conviterliudpod.csv conviterliudpodold.csv conviterliudpodk10.csv conviterliudpodk10old.csv conviterliudpodk100.csv conviterliudpodk100old.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
#
#set out 'liu_all.png'
#list='conviterliu.csv conviterliuold.csv conviterliud100.csv conviterliud100old.csv conviterliudk100.csv conviterliudk100old.csv conviterliud10.csv conviterliud10old.csv conviterliudk10.csv conviterliudk10old.csv conviterliudpod.csv conviterliudpodold.csv conviterliudpodk10.csv conviterliudpodk10old.csv conviterliudpodk100.csv conviterliudpodk100old.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
#
#
#set out 'liu_old.png'
#list='conviterliu.csv conviterliuold.csv conviterliudsnap.csv conviterliudsnapold.csv conviterliudpod.csv conviterliudpodold.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
#
#set out 'liu_told.png'
#list='conviterliu.csv conviterliut.csv conviterliutold.csv conviterliutdpod.csv conviterliutdpodold.csv conviterliutdsnap.csv conviterliutdsnapold.csv'
#plot for [file in list] file u ($1 <= 0 ? NaN : $1):(sqrt($3)) w l t file
######
