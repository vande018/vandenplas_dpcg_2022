push!(LOAD_PATH,"../julia_blup_lib/")

using LinearAlgebra
using SparseArrays
using DelimitedFiles
using Statistics
using modgeneral
using modblup
using modpedigree
using modsolver
using Random
using Clustering
using Arpack
using TimerOutputs
using modpreconditioner

##################################################
const to = TimerOutput()

maxiter = 2000

#data
f=open("param")

fdataeff = readline(f) #pheno.eff
fdataphen = readline(f) #pheno.phen
fped = readline(f) #ped_nogg.dat
fgenid = readline(f) #geno.id
fgen = readline(f) #geno.dat
fvare = readline(f)
fvarg = readline(f)
fpfix = readline(f)
fpan = readline(f)
fpalpha = readline(f)
close(f)

println("\nStart reading pedigree file")
@time ped = readdlm(fped, ' ', Int64, header=false)

println("\nStart reading effect file")
@time effect = readdlm(fdataeff, ' ', Int64, header=false)

println("\nStart reading phenotype file")
@time phen_ = readdlm(fdataphen, ' ', Float64, header=false)

phen = vec(phen_[:,1])

println("\nStart reading genotype files")
@time (idgen, geno, ngen, nsnp) = readgeno(fgenid, fgen)

vare = parse(Float64, fvare)
printmat(vare, "VarE ")

varg = parse(Float64, fvarg)
printmat(varg, "VarG ")

alpha = parse(Float64, fpalpha)
printmat(alpha, "Alpha (proportion of residual polygenic variance) ")

#tmp = parse(Int64, fpfix)
#p_fixed = [tmp]
tmp = split(fpfix)

p_fixed = map(x -> parse(Int64, x), tmp)
printmat(p_fixed, "Position fixed ")

p_an = parse(Int64, fpan)
printmat(p_an, "Position animal ")

printmat(ngen, "Number of genotyped animals     : ")
printmat(nsnp, "Number of SNPs (before cleaning): ")

##################################################

reduction = 0 #between 0-1 included
reduced_size_pheno = Int(floor(size(effect, 1) * (1 - reduction)))
reduced_size_geno = Int(floor(size(idgen, 1) * (1 - reduction)))

effect = effect[1:reduced_size_pheno, :]
println("Reduced effect size = ", size(effect))
phen = phen[1:reduced_size_pheno]
println("Reduced phenotype size = ", size(phen))
idgen = idgen[1:reduced_size_geno]
println("Reduced ID genotype size = ", size(idgen))

##################################################

#Matrix assembly
#println("\nCreate pedigree relationship matrix")
#@time (A,inb) = createa(ped)

println("\nCompute inbreeding coefficients")
@time inbr = inbreeding(ped)

println("\nCreate inverted pedigree relationship matrix")
@time Ai = createai(ped, inbr)

println("\nCreate incidence matrices")
println("\n Create the incidence matrix for the effect ", p_fixed[1])
@time X = createincidencematrix(effect, p_fixed[1])
if size(p_fixed,1) > 1
    for i = 2:size(p_fixed, 1)
    global X
    println("\n Loop: Create the incidence matrix for the effect ", p_fixed[i])
    @time xx = createincidencematrix(effect, p_fixed[i])
    X = [X xx]
    end
end
println("\n Incidence matrix X: ",summary(X))

println("\n Incidence matrix for the animal effect")
@time W = createincidencematrix(effect, p_an, maximum(ped[:,1]))
println("\n Incidence matrix W: ",summary(W))

println("\nCompute scaled genotype matrix Z, allele frequencies p, and sum2pq")
@time (Z, p, sumpq, nsnp) = creategeno(geno)
printmat(nsnp, "Number of SNPs (after cleaning): ")

Z = Z[1:reduced_size_geno, :]
println("Reduced SNP genotype matrix size = ", size(Z))

##################################################
println("\nCreate LHS and RHS for Liu et al. ssSNPBLUP")
@time (lhsliu, rhsliu) = createliublup(phen, idgen, X, W, Z, Ai, vare, varg, alpha, sumpq)

println("Non-zero values in LHS = ", nnz(lhsliu))
println("Density = ", convert(Float64, nnz(lhsliu)) / convert(Float64, size(lhsliu,1)^2))

##################################################
println("\nSolve ssLiuBLUP by PCG")

name = "pcg_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliu, iterliu, conviterliu, eigestimatesliu) = pcg(name, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)

writedlm("conviterliu.csv", conviterliu, ',')

##################################################
println("\nSolve ssLiuBLUP by PCG with a block-preconditioner")

name = "bpcg_"

@timeit to name * "M" begin
# m1 = sparse(Diagonal(lhsliu))
 nend = size(lhsliu,1)
# nstart = nend - size(Z, 2) + 1
 nstart = nend - size(Z, 2)
# m1[nstart : nend, nstart : nend] = lhsliu[nstart : nend, nstart : nend]

 fact = bunchkaufman(Symmetric(Matrix(lhsliu[nstart+1 : nend, nstart+1 : nend])))
 m2 = blockjacobi(nstart, nend, sparse(Diagonal(lhsliu[1:nstart, 1:nstart])), fact)

end

init_sol = zeros(size(rhsliu))
@timeit to name (solliub, iterliub, conviterliub, eigestimatesliub) = pcg(name, lhsliu, rhsliu, 1e-12, maxiter, m2, init_sol, to)

writedlm("conviterliub.csv", conviterliub, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 10 SNPs")

name = "dpcg_10_"

nsnppersubd = 10 
@timeit to name * "defmat" defmat = createdefmat_vdp2018(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2))

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliud10, iterliud10, conviterliud10, eigestimatesliud10) = dpcg(name, defmat, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliud10.csv", conviterliud10, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 100 SNPs")

name = "dpcg_100_"

nsnppersubd = 100
@timeit to name * "defmat" defmat = createdefmat_vdp2018(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2))

@timeit to name *  "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliud100, iterliud100, conviterliud100, eigestimatesliud100) = dpcg(name, defmat, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliud100.csv", conviterliud100, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Kmeans 10 SNPs")

name = "dpcg_K10_"

nsnppersubd = 10
ncluster = Int.(floor(nsnp / nsnppersubd))
if (nsnp % nsnppersubd) != 0
    ncluster += 1
end
printmat(ncluster," Number of clusters (Kmeans): ")

if ncluster < 1
    ncluster = 1
end
@timeit to name * "defmat" defmat_kmeans10 = createdefmat_kmeans(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2), Z, ncluster, 200)

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudk10, iterliudk10, conviterliudk10, eigestimatesliudk10) = dpcg(name, defmat_kmeans10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)

writedlm("conviterliudk10.csv", conviterliudk10, ',')

##################################################
println("\nSolve ssLiuBLUP by DPCG using Kmeans 100 SNPs")

name = "dpcg_K100_"

nsnppersubd = 100
ncluster = Int.(floor(nsnp / nsnppersubd))
if (nsnp % nsnppersubd) != 0
    ncluster += 1
end
printmat(ncluster," Number of clusters (Kmeans): ")

if ncluster < 1
    ncluster = 1
end
@timeit to name * "defmat" defmat_kmeans100 = createdefmat_kmeans(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2), Z, ncluster, 200)

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudk100, iterliudk100, conviterliudk100, eigestimatesliudk100) = dpcg(name, defmat_kmeans100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudk100.csv", conviterliudk100, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 10 SNPs - reduced scenario")

nred = 12
red_array = zeros(nred)
for i = 1:nred
 red_array[i] = i * 0.25/100.
end

printmat(red_array, "Reduction array")
solred = zeros(size(rhsliu))

name = "tmp_"

for i = 1:nred
   local to_tmp = TimerOutput()

   local reduction = red_array[i] #0 #0-1
   printmat(reduction, "Current reduction: ")

   local reduced_size_pheno = Int(floor(size(effect, 1) * (1 - reduction)))
   local reduced_size_geno = Int(floor(size(idgen, 1) * (1 - reduction)))
   
   effect_red = effect[1:reduced_size_pheno, :]
   println("Reduced effect size = ", size(effect_red))
   phen_red = phen[1:reduced_size_pheno, :]
   println("Reduced phenotype size = ", size(phen_red))
   idgen_red = idgen[1:reduced_size_geno]
   println("Reduced ID genotype size = ", size(idgen_red))
   
   println("\nCreate incidence matrices - reduced")
   println("\n Create the incidence matrix for the effect ", p_fixed[1])
   @time X_red = createincidencematrix(effect_red, p_fixed[1])
   if size(p_fixed,1) > 1
       for i = 2:size(p_fixed, 1)
#        global X_red
        println("\n Loop: Create the incidence matrix for the effect ", p_fixed[i])
        @time xx_red = createincidencematrix(effect_red, p_fixed[i])
        X_red = [X_red xx_red]
       end
   end
   println("\n Incidence matrix X - reduced: ",summary(X_red))
   
   println("\n Incidence matrix for the animal effect - reduced")
   @time W_red = createincidencematrix(effect_red, p_an, maximum(ped[:,1]))
   println("\n Incidence matrix W_red: ",summary(W_red))
   
   println("\nCompute scaled genotype matrix Z reduced")
   printmat(nsnp, "Number of SNPs (after cleaning): ")
   
   Z_red = Z[1:reduced_size_geno, :]
   println("Reduced SNP genotype matrix size = ", size(Z_red))
   
   ##################################################
   println("\nCreate LHS and RHS for Liu et al. ssSNPBLUP for reduced scenario")
   @time (lhsliu_red, rhsliu_red) = createliublup(phen_red, idgen_red, X_red, W_red, Z_red, Ai, vare, varg, alpha, sumpq)
   
   ##################################################
   println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 10 SNPs -reduced")
   
   local nsnppersubd = 10 
   local defmat = @time createdefmat_vdp2018(size(lhsliu_red, 1), nsnppersubd, size(X_red, 2) + size(W_red, 2))
   
   local m1 = sparse(Diagonal(lhsliu_red))
   local init_sol = zeros(size(rhsliu_red))
   (solliud10_red, iterliud10_red, conviterliud10_red, eigestimatesliud10_red) = dpcg(name, defmat, lhsliu_red, rhsliu_red, 1e-12, maxiter, m1, init_sol, to_tmp) 

   global solred
   solred = [solred solliud10_red]

end

solred = solred[:, 2:size(solred,2)]

println("\n Solution matrix: ",summary(solred))


##################################################
println("\nSolve ssLiuBLUP by PCG - old")

name = "pcg_old_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliuold, iterliuold, conviterliuold, eigestimatesliuold) = pcg(name, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliuold.csv", conviterliuold, ',')


##################################################
println("\nSolve ssLiuBLUP by PCG with a block-preconditioner - old")

name = "bpcg_old_"

@timeit to name * "M" begin
# m1 = sparse(Diagonal(lhsliu))
 nend = size(lhsliu,1)
# nstart = nend - size(Z, 2) + 1
 nstart = nend - size(Z, 2)
# m1[nstart : nend, nstart : nend] = lhsliu[nstart : nend, nstart : nend]

 fact = bunchkaufman(Symmetric(Matrix(lhsliu[nstart+1 : nend, nstart+1 : nend])))
 m2 = blockjacobi(nstart, nend, sparse(Diagonal(lhsliu[1:nstart, 1:nstart])), fact)

end

init_sol = solred[:,1]
@timeit to name (solliubold, iterliubold, conviterliubold, eigestimatesliubold) = pcg(name, lhsliu, rhsliu, 1e-12, maxiter, m2, init_sol, to)

writedlm("conviterliubold.csv", conviterliubold, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 10 SNPs - old")

name = "pcg_old10_"

nsnppersubd = 10 
@timeit to name * "defmat" defmat = createdefmat_vdp2018(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2))

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliud10old, iterliud10old, conviterliud10old, eigestimatesliud10old) = dpcg(name, defmat, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliud10old.csv", conviterliud10old, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Vandenplas et al. (2018) - 100 SNPs - old")

name = "pcg_old100_"

nsnppersubd = 100
@timeit to name * "defmat" defmat = createdefmat_vdp2018(size(lhsliu, 1), nsnppersubd, size(X, 2) + size(W, 2))

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliud100old, iterliud100old, conviterliud100old, eigestimatesliud100old) = dpcg(name, defmat, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliud100old.csv", conviterliud100old, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Kmeans 10 SNPs - old")

name = "pcg_oldK10_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudk10old, iterliudk10old, conviterliudk10old, eigestimatesliudk10old) = dpcg(name, defmat_kmeans10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudk10old.csv", conviterliudk10old, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using Kmeans 100 SNPs")

name = "pcg_oldK100_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudk100old, iterliudk100old, conviterliudk100old, eigestimatesliudk100old) = dpcg(name, defmat_kmeans100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudk100old.csv", conviterliudk100old, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using POD")

name = "dpcg_pod_"

matrix_type = "data"
@timeit to name * "defmat" defmat_pod = createdefmat_pod_choice(solred, matrix_type)

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudpod, iterliudpod, conviterliudpod, eigestimatesliudpod) = dpcg(name, defmat_pod, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpod.csv", conviterliudpod, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using POD - old")

name = "dpcg_podold_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudpodold, iterliudpodold, conviterliudpodold, eigestimatesliudpodold) = dpcg(name, defmat_pod, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpodold.csv", conviterliudpodold, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using POD + Kmeans 10")

name = "dpcg_podK10_"

@timeit to name * "defmat" defmat_podk10 = [defmat_pod defmat_kmeans10]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudpodk10, iterliudpodk10, conviterliudpodk10, eigestimatesliudpodk10) = dpcg(name, defmat_podk10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpodk10.csv", conviterliudpodk10, ',')



##################################################
println("\nSolve ssLiuBLUP by DPCG using POD + Kmeans 100")

name = "dpcg_podK100_"

@timeit to name * "defmat" defmat_podk100 = [defmat_pod defmat_kmeans100]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudpodk100, iterliudpodk100, conviterliudpodk100, eigestimatesliudpodk100) = dpcg(name, defmat_podk100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpodk100.csv", conviterliudpodk100, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using POD + Kmeans 10 + old")

name = "dpcg_podK10old_"

@timeit to name * "defmat" defmat_podk10 = [defmat_pod defmat_kmeans10]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudpodk10old, iterliudpodk10old, conviterliudpodk10old, eigestimatesliudpodk10old) = dpcg(name, defmat_podk10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpodk10old.csv", conviterliudpodk10old, ',')



##################################################
println("\nSolve ssLiuBLUP by DPCG using POD + Kmeans 100 + old")

name = "dpcg_podK100old_"

@timeit to name * "defmat" defmat_podk100 = [defmat_pod defmat_kmeans100]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudpodk100old, iterliudpodk100old, conviterliudpodk100old, eigestimatesliudpodk100old) = dpcg(name, defmat_podk100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudpodk100old.csv", conviterliudpodk100old, ',')






##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots")

name = "dpcg_snap_"

matrix_type = "data"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudsnap, iterliudsnap, conviterliudsnap, eigestimatesliudsnap) = dpcg(name, solred, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnap.csv", conviterliudsnap, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots - old")

name = "dpcg_snapold_"

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudsnapold, iterliudsnapold, conviterliudsnapold, eigestimatesliudsnapold) = dpcg(name, solred, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnapold.csv", conviterliudsnapold, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots + Kmeans 10")

name = "dpcg_snapoldk10_"

@timeit to name * "defmat" defmat_snapk10 = [solred defmat_kmeans10]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudsnapk10, iterliudsnapk10, conviterliudsnapk10, eigestimatesliudsnapk10) = dpcg(name, defmat_snapk10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnapk10.csv", conviterliudsnapk10, ',')



##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots + Kmeans 100")

name = "dpcg_snapoldk100_"

@timeit to name * "defmat" defmat_snapk100 = [solred defmat_kmeans100]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = zeros(size(rhsliu))
@timeit to name (solliudsnapk100, iterliudsnapk100, conviterliudsnapk100, eigestimatesliudsnapk100) = dpcg(name, defmat_snapk100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnapk100.csv", conviterliudsnapk100, ',')


##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots + Kmeans 10 + old")

name = "dpcg_snapoldk10old_"

@timeit to name * "defmat" defmat_snapk10 = [solred defmat_kmeans10]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudsnapk10old, iterliudsnapk10old, conviterliudsnapk10old, eigestimatesliudsnapk10old) = dpcg(name, defmat_snapk10, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnapk10old.csv", conviterliudsnapk10old, ',')



##################################################
println("\nSolve ssLiuBLUP by DPCG using snapshots + Kmeans 100 + old")

name = "dpcg_snapoldk100old_"

@timeit to name * "defmat" defmat_snapk100 = [solred defmat_kmeans100]

@timeit to name * "M" m1 = sparse(Diagonal(lhsliu))
init_sol = solred[:,1]
@timeit to name (solliudsnapk100old, iterliudsnapk100old, conviterliudsnapk100old, eigestimatesliudsnapk100old) = dpcg(name, defmat_snapk100, lhsliu, rhsliu, 1e-12, maxiter, m1, init_sol, to)
writedlm("conviterliudsnapk100old.csv", conviterliudsnapk100old, ',')

##################################################
io = open("time.dat", "w");
show(io, to; compact = false, allocations = false, sortby = :firstexec , linechars = :ascii )
close(io);

##################################################

allsol = [iterliu eigestimatesliu 
iterliub eigestimatesliub
iterliud10 eigestimatesliud10 
iterliud100 eigestimatesliud100 
iterliudk10 eigestimatesliudk10 
iterliudk100 eigestimatesliudk100 
iterliuold eigestimatesliuold
iterliubold eigestimatesliubold 
iterliud10old eigestimatesliud10old 
iterliud100old eigestimatesliud100old 
iterliudk10old eigestimatesliudk10old 
iterliudk100old eigestimatesliudk100old 
iterliudpod eigestimatesliudpod 
iterliudpodold eigestimatesliudpodold 
iterliudpodk10 eigestimatesliudpodk10 
iterliudpodk100 eigestimatesliudpodk100 
iterliudpodk10old eigestimatesliudpodk10old 
iterliudpodk100old eigestimatesliudpodk100old
iterliudsnap eigestimatesliudsnap 
iterliudsnapold eigestimatesliudsnapold 
iterliudsnapk10 eigestimatesliudsnapk10 
iterliudsnapk100 eigestimatesliudsnapk100 
iterliudsnapk10old eigestimatesliudsnapk10old 
iterliudsnapk100old eigestimatesliudsnapk100old]

writedlm("allsol.csv", allsol, ',')

##################################################
println("\nComparison of the number of iterations")
printmat(iterliu, "ssSNPBLUP_PCG")
printmat(iterliub, "ssSNPBLUP_PCGb")
printmat(iterliud10, "ssSNPBLUP_DPCG 10")
printmat(iterliud100, "ssSNPBLUP_DPCG 100")
printmat(iterliudk10, "ssSNPBLUP_DPCG K10")
printmat(iterliudk100, "ssSNPBLUP_DPCG K100")

printmat(iterliuold, "ssSNPBLUP_PCG - old")
printmat(iterliubold, "ssSNPBLUP_PCGb - old")
printmat(iterliud10old, "ssSNPBLUP_DPCG 10 - old")
printmat(iterliud100old, "ssSNPBLUP_DPCG 100 - old")
printmat(iterliudk10old, "ssSNPBLUP_DPCG K10 - old")
printmat(iterliudk100old, "ssSNPBLUP_DPCG K100 - old")

printmat(iterliudpod, "ssSNPBLUP_DPCG POD")
printmat(iterliudpodold, "ssSNPBLUP_DPCG POD - old")


printmat(iterliudpodk10, "ssSNPBLUP_DPCG POD + Kmeans 10")
printmat(iterliudpodk100, "ssSNPBLUP_DPCG POD + Kmeans 100")

printmat(iterliudpodk10old, "ssSNPBLUP_DPCG POD + Kmeans 10 + old")
printmat(iterliudpodk100old, "ssSNPBLUP_DPCG POD + Kmeans 100 + old")


printmat(iterliudsnap, "ssSNPBLUP_DPCG POD")
printmat(iterliudsnapold, "ssSNPBLUP_DPCG POD - old")


printmat(iterliudsnapk10, "ssSNPBLUP_DPCG POD + Kmeans 10")
printmat(iterliudsnapk100, "ssSNPBLUP_DPCG POD + Kmeans 100")

printmat(iterliudsnapk10old, "ssSNPBLUP_DPCG POD + Kmeans 10 + old")
printmat(iterliudsnapk100old, "ssSNPBLUP_DPCG POD + Kmeans 100 + old")




