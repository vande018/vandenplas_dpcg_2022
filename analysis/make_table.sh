#!/bin/bash

tfile="time.dat"

paste -d' ' name_tmp.tex <(awk -F',' '{$2=$2*1000; printf "%.0f & $%.3f*10^{-3}$ & %.3f \\\\\n", $1,$2,$3}' allsol.csv) >table

paste -d'&' name_tmp.tex \
<(grep "_M " $tfile |awk '{print $5}' ) \
<(grep "pcg" $tfile |grep -v "_Ap" | grep -v "_defmat" | grep -v "_M" |grep -v "Setup" | grep -v "_iter" |grep -v "Pw" |awk '{print $5}') \
<(grep "_iter " $tfile |awk '{print $5}') \
<(grep "_Mir " $tfile |awk '{print $5}') \
| sed 's/&/ & /g' >table_time

paste -d'&' name_tmp_sub.tex \
<(grep "_Setup_P " $tfile |awk '{print $5}') \
<(grep "_Pw " $tfile |awk '{print $5}') \
| sed 's/&/ & /g' >table_time_sub
