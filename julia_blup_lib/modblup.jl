module modblup

using SparseArrays
using LinearAlgebra
using modgeneral
using DelimitedFiles
using Glob

export createincidencematrix,creategeno,createliublup



function createincidencematrix(effectmat,eff,maxval=1)
    nobs = size(effectmat,1)
    nlevel = maximum(effectmat[:,eff])

    println("  Number of levels for effect ",eff,": ",nlevel)

    if nlevel < maxval
    nlevel = maxval
    end

    x = spzeros(nobs,nlevel)

    for i = 1:nobs
    x[i,effectmat[i,eff]] = 1.
    end

    return x
end

function creategeno(mat::Array{Float64,2}, maf = 0.01, nsel = 0)
 #Compute the centered genotyped matrix as x = mat-2*p'
 #  p = allele frequencies
 #  s2pq = sum(2(p(1-p))
 #Only the SNPs with maf < p < (1.-maf) will be included in the genotype matrix
     
    nobs = size(mat,1)
    nsnp = size(mat,2)

    if nsel > 0
    nsnp = nsel
    end

    ptmp = (ones(1,nobs) * mat[1:nobs, 1:nsnp])' / (2.0*nobs)

    f(x) = ( x .> maf && x .< (1. - maf)) #keep all SNPs which has a higher frequency than the minor allele frequency (maf)

    indx = getindex.(findall(f, ptmp), [1])

    nsnp = size(indx,1)

    p = ptmp[indx]

    x = mat[1:nobs, indx] - 2.0 * ones(nobs,1) * p'

    sumpq = 2.0 * p' * (ones(size(p,1),1) - p)

    return x, p, sumpq[1], nsnp
end

function createliublup(y,id,X,W,Z,Ai,vare,varg,alpha,sumpq)
 #Set up the coefficient matrix and the right-hand-side for single-step  SNPBLUP following Liu et al. (2014)

 ve = 1. / vare
 vg = 1. / varg

 nx = size(X,2)
 nz = size(Z,2)
 nw = size(W,2)   #number of animals

 nid = setdiff(1:nw,id)

 idx = id .+ nx
 nidx = nid .+ nx

 tt = nw + nz
 tot = tt + nx
 lhs = spzeros(tot,tot)


 Ai11 = Ai[nid,nid]

 F = ldlt(Ai11)
# Atemp = F\Ai12
# Q = *(Ai12',Atemp)

 Ai12 = Ai[nid,id]
 Ai21 = Ai[id,nid]

 ng = size(id,1)
 Q = zeros(ng,ng)
 for i in 1:1000:ng
  nmax = min( i+1000-1, ng)
  Atemp = F\Ai12[:,i:nmax]
  Q[:,i:nmax] = *(Ai21, Atemp)
 end

 #Hi = spzeros(tt,tt)
 #Hi[id,id] = Hi[id,id] -((1.0/alpha)-1.0) * Q;
 lhs[idx,idx] = lhs[idx,idx] - (((1.0/alpha)-1.0) * vg) * Q;

 Ai22 = Ai[id,id]

 #Hi[id,id] += ((1.0/alpha)-1.0) * Ai22;
 lhs[idx,idx] += (((1.0/alpha)-1.0)* vg) * Ai22;

 #Hi[nw+1:tt,nw+1:tt] += (speye * sumpq/(1.0 - alpha)) + Z' * ttt / alpha

 ttt = Ai22 * Z - Q * Z
 lhs[nx+nw+1:nx+tt,nx+nw+1:nx+tt] +=  Z' * ttt * (vg / alpha)

 #Hi[nw+1:tt,id] += (-1.0/alpha) * ttt' 
 lhs[nx+nw+1:nx+tt,idx] += (- vg / alpha) * ttt' 
 #Hi[id,nw+1:tt] += (-1.0/alpha) * ttt
 lhs[idx,nx+nw+1:nx+tt] += (- vg / alpha) * ttt

 speye = spzeros(nz,nz)
 speye += sparse(I,nz,nz)
 lhs[nx+nw+1:nx+tt,nx+nw+1:nx+tt] += (speye * vg * sumpq/(1.0 - alpha))

 #Hi[1:nw,1:nw] += Ai
 lhs[nx+1:nx+nw,nx+1:nx+nw] += Ai * vg

 #lhs[nx+1:tot,nx+1:tot] += Hi * vg

 WZ1 = spzeros(size(W,1),nz)
 inc = [X W WZ1]

 lhs += inc' * inc * ve

 rhs = inc' * y * ve

 printmat(size(lhs,1)," Number of equations: ")

 return lhs,rhs

end

end

