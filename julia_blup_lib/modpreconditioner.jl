module modpreconditioner

using LinearAlgebra
using SparseArrays
using modgeneral

export blockjacobi, \

mutable struct blockjacobi
 n::Int
 ntot::Int
 sp::SparseMatrixCSC{Float64, Int64}
 fact::BunchKaufman{Float64, Matrix{Float64}}
end


import Base.(\)
function (\)(self::blockjacobi, b::AbstractVector)

 r = copy(b)
 r[1:self.n] = self.sp \ b[1:self.n]
 r[self.n+1:self.ntot] = \(self.fact, b[self.n+1:self.ntot])

 return r
end


end #module
