module modsolver

using LinearAlgebra
using SparseArrays
using modgeneral
using Arpack
using Random
using StatsBase
using Clustering
using Plots
using DelimitedFiles
using Statistics
using TimerOutputs
using modpreconditioner

export pcg, pcgl, dpcg, createdefmat_kmeans, createdefmat_vdp2018, createdefmat_pod_choice

##################################################

function pcg(name, A, b, tol, maxit, m1, x0, to)
    T = zeros(maxit, maxit)

    p = zeros(size(b))
    oldtau = 1

    x = x0
    r = b - A*x

    b_norm = norm(b)^2
    res = norm(r)^2
    alpha = 1

    conviter = zeros(maxit, 3)
    conviter[1, :] = [1 sqrt(res) res/b_norm]

    iter = 0

    thr = tol * b_norm
 
    while res > thr && iter < maxit
      @timeit to name * "iter" begin
        iter += 1
        @timeit to name * "Mir" z = m1 \ r
        tau = dot(z, r)
        beta = tau / oldtau
        oldtau = tau
        p = z + beta * p
        @timeit to name * "Ap" w = A * p
#         Needed only for eigest.
        oldalpha = alpha
        d = dot(p, w)
        alpha = tau / d
        x += alpha * p
#        if (iter-1) % 50 == 0    #-1 to be equivalent to mypcgconv.m
#          r =b- A * x
#        else
        r -= alpha * w
#        end
        if iter > 1
            T[iter-1:iter, iter-1:iter] = T[iter-1:iter, iter-1:iter] + [1 sqrt(beta); sqrt(beta) beta] ./ oldalpha 
        end
        res = norm(r)^2
        conv = res / b_norm
        conviter[iter, :] = [iter sqrt(res) conv]
        println("round = ", iter, " convergence = ", conv)
      end
    end

    if iter > 1
        T = T[1:iter-1, 1:iter-1]
        l = eigvals(T)
        eigest = [minimum(l) maximum(l)]
#         fprintf (stderr, "pcg condest: %g\n", eigest(2)/eigest(1));
    else
        eigest = ones(2, 1)
    end

    return x, iter, conviter, eigest
end

##################################################

function pcgl(A, b, tol, maxit, m1, x0, lup)
    T = zeros(maxit, maxit)

    p = zeros(size(b))
    oldtau = 1

    x = deepcopy(x0)

    b0 = lup \ b

    x1 = lup' \ x
    x2 = A * x1
    x3 = lup \ x2

    r = b0 - x3

    b_norm = norm(b0)^2
    res = norm(r)^2
    alpha = 1

    conviter = zeros(maxit, 3)
    conviter[1, :] = [1 sqrt(res) res/b_norm]

    iter = 0

    thr = tol * b_norm

    while res > thr && iter < maxit
        iter += 1
        z = m1 \ r
        tau = dot(z, r)
        beta = tau / oldtau
        oldtau = tau
        p = z + beta * p
#         w = A * p
        x1 = lup' \ p
        x2 = A * x1
        w = lup \ x2
#         Needed only for eigest.
        oldalpha = alpha
        d = dot(p, w)
        alpha = tau / d
        x += alpha * p
#         if (iter-1)%50 == 0    #-1 to be equivalent to mypcgconv.m
#             r =b0- A * x
#         else
        r -= alpha * w
#         end
        if iter > 1
            T[iter-1:iter, iter-1:iter] = T[iter-1:iter, iter-1:iter] + [1 sqrt(beta); sqrt(beta) beta] ./ oldalpha 
        end
        res = norm(r)^2
        conv = res / b_norm
        conviter[iter, :] = [iter sqrt(res) conv]
        println("round = ", iter, " convergence = ", conv)
    end


    if iter > 1
        T = T[1:iter-1, 1:iter-1]
        l = eigvals(T)
        eigest = [minimum(l) maximum(l)]
#         fprintf (stderr, "pcg condest: %g\n", eigest(2)/eigest(1));
    else
        eigest = ones(2, 1)
    end

    x = lup' \ x

    return x, iter, conviter, eigest
end

##################################################

function createdefmat_vdp2018(neq, nsnppersubd, nstart) #original code
     jj = (neq - nstart) % nsnppersubd  #modulo

     nadd = 0
     if jj != 0
         nadd = 1
     end

     nsubdomain = floor((neq-nstart)/nsnppersubd) + nadd + 1
     nn = Int.(nsubdomain)

     printmat(nn," Number of subdomains: ")

     defmat = spzeros(neq, nn)
     defmat[1:nstart, 1] .= 1.

     indx = zeros(Int, neq - nstart)

     n = 1
     for i = 1 : nsnppersubd : neq - nstart - jj
         n += 1
         indx[i:i + nsnppersubd - 1] .= n
     end

     indx[neq - nstart - jj + 1 : neq - nstart] .= nn

     Random.seed!(0) #set seed
     indx = shuffle!(indx)

     for i = 1:(neq - nstart)
	 defmat[nstart + i, indx[i] ] = 1.
     end

#     n = 1
#     for i = nstart+1:nsnppersubd:neq - jj
#         n += 1
#         defmat[i:i + nsnppersubd - 1, n] .= 1.
#     end
#
#     defmat[neq- jj:neq, nn] .= 1.

     return defmat
 end

##################################################

function createdefmat_kmeans(neq, nsnppersubd, nstart, Z, ncluster, maxit)
    Random.seed!(0)
    
    println("\nCompute the covariance matrix")
    cov_matrix = @time Z' * Z
    println("   ", summary(cov_matrix))

    println("\nCompute the standard deviation matrix")
    std_matrix = @time sparse(Diagonal(cov_matrix))
    
    for i in 1:size(std_matrix, 1)
        std_matrix[i, i] ^= -0.5
    end
    
    println("\nCompute the correlation matrix")
    cor_matrix = @time std_matrix * cov_matrix * std_matrix

    println("\nInitiate k-means algorithm")
#     cluster_centres = Random.shuffle(1:size(Z, 2))[1:ncluster] # Int.(floor.(collect(range(1, stop = size(Z, 2), length = ncluster)))) 
    cluster_centres = initseeds(KmppAlg(), cor_matrix, ncluster) #k-means++
    
    kmeans_result = @time kmeans(cor_matrix, ncluster; init = cluster_centres, maxiter = maxit, display=:iter)
    
    @assert nclusters(kmeans_result) == ncluster

    kmeans_assignment = assignments(kmeans_result)

    cluster_sizes = counts(kmeans_result)

    printmat(cluster_sizes, " cluster_size: ")
    
    cluster_index = [0]
    
    for cluster_size in cluster_sizes
        push!(cluster_index, cluster_index[end] + cluster_size)
    end

    permutations = sortperm(kmeans_assignment)
    permutations .+= nstart
    
    nsubdomain = ncluster + 1

    printmat(nsubdomain, " Number of subdomains (Kmeans): ")

    defmat = spzeros(neq, nsubdomain)
    defmat[1:nstart, 1] .= 1.

    col = 1
    for i = 1:ncluster
        col += 1
        defmat[permutations[cluster_index[i]+1:cluster_index[i+1]], col] .= 1.
    end
    
    return defmat
end

##################################################

 function createdefmat_pod_choice(X, matrix) #POD choose matrix
    
     if matrix == "data"
         R = (X' * X) ./ (size(X, 2) - 1)
     elseif matrix == "covariance"
         X_mean = mean(X, dims = 2)
         X = (X - X_mean * ones(1, size(X, 2))) 
         R = (X' * X) ./ (size(X, 2) - 1)
     elseif matrix == "correlation"
         X_mean = mean(X, dims = 2)
         X = (X - X_mean * ones(1, size(X, 2)))
         R = X' * X 

         X_std = zeros(size(X, 2))
         for i in 1:size(X, 2)
             if abs(R[i, i]) > 1e-16
                 X_std[i] = R[i, i]^(-0.5)
             else
                 X_std[i] = 0
             end
         end
         X_std = Diagonal(X_std)

         R = (X_std * R * X_std) ./ (size(X, 2) - 1) 
     end
            
     R = svd(R)
    
     println("Singular values of data matrix")
     println(R.S)
    
     sv_ratio = R.S ./ sum(R.S) 
     println(sv_ratio)
    
     p = 2 #size(X, 2) #0
    
     println("Deflation vectors used = ", p)
    
     S_sqrt_inverse = pinv(Diagonal(R.S)).^(0.5)
    
     return (X * R.V * S_sqrt_inverse)[:, 1:p]
 end

##################################################

function dpcg(name, Z, A, b, tol, maxit, m1, x0, to)
  @timeit to name * "Setup_P" begin
#     E=Z'*A*Z;Q=Z*inv(E)*Z';P=I-A*Q
    AZ = A * Z
    E = Z' * AZ 
#     %v=E\x;
#     Py=y-(AZ(E\(Z'y)))

#     printdense(E,"\n E= ")
    count = 0
    for i = 1:size(E, 1) #add count 
        if E[i,i] == 0 
            E[i,i] = 0.0001
            count += 1
        end
    end
  end
    println("Change diagonal of E = ", count)

    T = zeros(maxit, maxit)

    p = zeros(size(b))
    oldtau = 1

    x=x0
    r = b - A*x

    #r=Pr
    y1 = Z' * r
    y2 = E \ y1
    r = r - AZ * y2

    b_norm = norm(b)^2
    res = 0
    res = norm(r)^2
    alpha = 1

    conviter = zeros(maxit, 3)
    conviter[1, :] = [1 sqrt(res) res/b_norm]

    iter = 0

    thr = tol * b_norm

    while res > thr && iter < maxit
      @timeit to name * "iter" begin
        iter += 1
        @timeit to name * "Mir" z = m1 \ r
        tau = dot(z, r)
        beta = tau / oldtau
        oldtau = tau
        p = z + beta * p 
        @timeit to name * "Ap" w = A * p
	@timeit to name * "Pw" begin
        y1 = Z' * w
        y2 = E \ y1
        w = w - AZ * y2
	end
#         Needed only for eigest.
        oldalpha = alpha
        d = dot(p, w)
        alpha = tau / d
        x += alpha * p
#         if (iter-1)%50 == 0    #-1 to be equivalent to mypcgconv.m
#             r = b - A * x
#         else
        r -= alpha * w
#         end
        if iter > 1
            T[iter-1:iter, iter-1:iter] = T[iter-1:iter, iter-1:iter] + [1 sqrt(beta); sqrt(beta) beta] ./ oldalpha 
        end
        res = norm(r)^2
        conv = res / b_norm
        conviter[iter, :] = [iter sqrt(res) conv]
        println("round = ", iter, " convergence = ", conv)
      end
    end

#   x=Qb +P'x
    y1 = A * x
#   x=Z*(E\(Z'*b))+x-Z*(E\(Z'*y1))
    x = x + Z * ( E \ (Z' * ( b - y1 ) ))

    if iter > 1
        T = T[1:iter-1, 1:iter-1]
        l= eigvals(T)
        eigest = [minimum(l) maximum(l)]
#         fprintf (stderr, "pcg condest: %g\n", eigest(2)/eigest(1));
    else
        eigest = ones(2, 1)
    end

    return x, iter, conviter, eigest
end

end #module
