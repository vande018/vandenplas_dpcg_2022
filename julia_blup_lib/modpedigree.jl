module modpedigree

using SparseArrays
using modgeneral

export printped,createa,createai,createl,inbreeding,aggbycolleau

function printped(ped)
 println("Pedigree ")
 for i=1:size(ped,1)
  println(ped[i,:])
 end
 println("")
end

function createa(ped)
 nb=size(ped,1)

 a=zeros(nb,nb)
 inb=zeros(nb)

 for i=1:nb
#  if i % 1000 == 0
#   println(i)
#  end
  if ped[i,2] >0
   a[i,1:i]=a[i,1:i]+a[ped[i,2],1:i]*0.5
  end
  if ped[i,3] >0
   a[i,1:i]=a[i,1:i]+a[ped[i,3],1:i]*0.5
  end
  a[i,i]=1
  if  ped[i,2] > 0 &&  ped[i,3]>0 
   s=ped[i,2]
   d=ped[i,3]
   inb[i]=0.5*a[d,s]
   a[i,i]=a[i,i]+inb[i]
  end
  a[1:i,i]=a[i,1:i]'
 end

 aa=sparse(a)
 return aa, inb

end

function createai(ped,inb)

 w=[1 -0.5 -0.5]
 nb=size(ped,1)
 ai=spzeros(nb,nb)

 for i=1:nb
  if i%10000 == 0
   println(i)
  end
  if ped[i,1] > 0
   mendel=1
   ms=1
   fs=0
   md=1
   fd=0
   if ped[i,2]>0
    ms=0
    fs=inb[ped[i,2]]
   end
   if ped[i,3] > 0
    md=0
    fd=inb[ped[i,3]]
   end
   mendel=4/((1+ms)*(1-fs)+(1+md)*(1-fd))
   for k=1:3
    for l=1:3
     if ped[i,l] >0 && ped[i,k] >0
      s=ped[i,l]
      d=ped[i,k]
      ai[s,d]=ai[s,d]+w[k]*w[l]*mendel
     end
    end
   end
  end
 end
 
 return ai

end

function createl(ped,inb)

 w=[1 -0.5 -0.5]
 nb=size(ped,1)
 ll=spzeros(nb,nb)
 mend=spzeros(nb,nb)
 
 for i=1:nb
  if i%10000 == 0
   println(i)
  end
  if ped[i,1] > 0
   mendel=1
   ms=1
   fs=0
   md=1
   fd=0
   if ped[i,2]>0
    ms=0
    fs=inb[ped[i,2]]
   end
   if ped[i,3] > 0
    md=0
    fd=inb[ped[i,3]]
   end
   mendel=4/((1+ms)*(1-fs)+(1+md)*(1-fd))
   mend[i,i]=mend[i,i]+sqrt(mendel)
   for k=1:3
    s=ped[i,k]
    if  s >0
     ll[s,i]=ll[s,i]+w[k]
    end

#    for l=1:3
#     if ped[i,l] >0 && ped[i,k] >0
#      s=ped[i,l]
#      d=ped[i,k]
#      ll[s,d]=ll[s,d]+w[k]*w[l]*mendel
#     end
#    end
   end
  end
 end
 
 ll=ll*mend

 return ll

end



function inbreeding(ped)
 
 nped=size(ped,1)

 L=zeros(nped+2)
 D=zeros(nped+2)
 F=zeros(nped+1)
 F[1]=-1.

 POINT=zeros(Int64,nped+2)
 for i=1:nped
  meuwluo(ped,i,F,L,D,POINT)
 end

 F = F[2:nped+1]
 return F
end

function meuwluo(ped,i,F,L,D,POINT)

 IS = ped[i,2]
 ID = ped[i,3]
 D[i+1] = 0.5-0.25 * ( F[IS+1] + F[ID+1] )    #within family variance
 if IS == 0 || ID == 0                        #non inbred animal
  F[i+1]=0.0
 else                                         #compute F(i)
  FI = -1.0                                   #put f=-1 and add diag of A:
  L[i+1] = 1.0                                #initialise row of L
  J = i                                       #j = oldest ancestor of i in list
  while J != 0                                #stop when list of ancestors is empty
   k=J                                        #K is a temporary variable
   R = 0.5 * L[k+1]                           #determine contribn to parents
   KS = min(ped[k,2],ped[k,3])                #Store parents so that
   KD = max(ped[k,2],ped[k,3])                #PED(i,3) > PED(i,2)
   if KD > 0                                  #find slot in link list for sire
    while POINT[k+1] > KD                     #stop if next ancestor
     k=POINT[k+1]                             #is older than sire
    end                                       #or is the sire
    L[KD+1] = L[KD+1] + R                     #add contribution to sire
    if KD != POINT[k+1]                       #include sire in link list
     POINT[KD+1] = POINT[k+1]                 #POINT(KS)=next ancest. in list
     POINT[k+1] = KD                          #POINT(previous ancestor) = sire
    end
    if KS > 0                                 #do the same for the dam
     while POINT[k+1] > KS                    #KS > KD, hence do not
      k = POINT[k+1]                          #need to reinitialise
     end                                      #variable K
     L[KS+1] = L[KS+1] + R
     if KS != POINT[k+1]
       POINT[KS+1] = POINT[k+1]
       POINT[k+1] = KS
     end
    end
   end
   FI = FI + L[J+1] * L[J+1] * D[J+1]          #add L*D*L value of animal j
   L[J+1] =0.0                                 #clear L(j) for next F evaluation
   k=J                                         #store 'old' J in temporary variable K
   J=POINT[J+1]                                # 'new' J = next oldest animal in list
   POINT[k+1]=0                                #clear POINT('old' J) for next F evaluation
  end
  F[i+1]=FI                                    #store F
 end

end

function aggbycolleau(ped,inb,id)

 nid=size(id,1)
 agg=zeros(nid,nid)
 nped=size(ped,1)

 di=zeros(nped)
 for i=1:nped
  s=ped[i,2]
  d=ped[i,3]
  countnnotzero = 0
  suminb=0.0
  if s == 0
   countnnotzero +=1
  else
   suminb += inb[s]
  end
  if d == 0
   countnnotzero +=1
  else
   suminb += inb[d]
  end
  di[i] = ( countnnotzero + 2. ) / 4.0 - 0.25 * suminb
 end

 for i=1:nid
  w=pedmattimesw(ped,nped,di,id[i])
  agg[:,i] = w[id]
 end
 
 sagg=sparse(agg)
 agg=0
 return sagg

end

function  pedmattimesw(ped,nped,di,id)
 
 w=zeros(nped)
 q=zeros(nped)
 q[id]=1.

 for i=id:-1:1
  if q[i] >0
   s=ped[i,3]
   d=ped[i,2]
   if s > 0
    q[s] = q[s] + q[i] *0.5
   end
   if d > 0
    q[d] = q[d] + q[i] *0.5
   end
  end
 end

 for i=1:nped
  s=ped[i,3]
  d=ped[i,2]
  tmp=0.0
  if s > 0
   tmp = tmp + w[s]
   w[i] = 0.5 *tmp
  end
  if d > 0
   tmp = tmp + w[d]
   w[i] = 0.5 *tmp
  end
  if q[i] >0.0
   w[i] = w[i] +di[i] * q[i]
  end
 end

 return w
end

end
