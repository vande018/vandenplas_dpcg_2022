module modgeneral

using DelimitedFiles

export printdense,printmat, readgeno

function printdense(x,name="")
 println()
 println(name)
 for i=1:size(x,1)
  println(x[i,:])
 end
 println("")
end

function printmat(x,name="")
 println()
 println(name)
 println(" ",summary(x), " :",repr(x))
end

function readgeno(fgenid,fgen)
 @time idgentmp = readdlm(fgenid, ' ', Int64, header=false)
 idgen = idgentmp[:,1]
 idgentmp = nothing

 #@time geno = readdlm(fgen, ' ', Float64, header=false)
 @time  gentmp = open(fgen,"r") do datafile
       [parse.(Float64,split(line)) for line in eachline(datafile)]
       end
 geno = permutedims(reshape(hcat(gentmp...), (length(gentmp[1]), length(gentmp))))
 gentmp = nothing

 ngen = size(geno,1)
 nsnp = size(geno,2)


 return idgen, geno, ngen, nsnp
end

end
