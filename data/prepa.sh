#!/bin/bash

nmax=5000000
nsnp=5000000

if [ "$#" -eq 2 ]
then
 echo "Maximum number of genotypes: $1"
 nmax=$1
 echo "Maximum number of SNPs     : $2"
 nsnp=$2
fi

datafile=$(pwd)'/../simulation/r_qmsim_bradford_2018/pop_data_001.txt'
genofile=$(pwd)'/../simulation/r_qmsim_bradford_2018/pop_mrk_001.txt'

>list.tmp

#top
prop='35'
for i in `seq 6 10`
do 
 awk 'NR>1{if ($5 == '$i' && $4 == "M") print $1,$5,$14}' $datafile |sed 's/+/ /g' |sort -k3,3nr >data.$i
 nline=`expr $(wc -l data.$i |cut -d' ' -f 1) / 2`
 nsel=$(( nline * prop / 100 ))
 echo $nline $nsel
 head -n $nline data.$i |sort -R |head -n $nsel |awk '{print $1}' >>list.tmp
done

prop='35'
for i in `seq 6 10`
do 
 awk 'NR>1{if ($5 == '$i' && $4 == "F") print $1,$5,$14}' $datafile |sed 's/+/ /g' |sort -k3,3nr >data.$i
 nline=`expr $(wc -l data.$i |cut -d' ' -f 1) / 2`
 nsel=$(( nline * prop / 100 ))
 echo $nline $nsel
 head -n $nline data.$i |sort -R |head -n $nsel |awk '{print $1}' >>list.tmp
done

#bottom
prop='10'
for i in `seq 6 10`
do 
 awk 'NR>1{if ($5 == '$i' && $4 == "M") print $1,$5,$14}' $datafile |sed 's/+/ /g' |sort -k3,3n >data.$i
 nline=`expr $(wc -l data.$i |cut -d' ' -f 1) / 2`
 nsel=$(( nline * prop / 100 ))
 echo $nline $nsel
 head -n $nline data.$i |sort -R |head -n $nsel |awk '{print $1}' >>list.tmp
done

prop='20'
for i in `seq 6 10`
do 
 awk 'NR>1{if ($5 == '$i' && $4 == "F") print $1,$5,$14}' $datafile |sed 's/+/ /g' |sort -k3,3n >data.$i
 nline=`expr $(wc -l data.$i |cut -d' ' -f 1) / 2`
 nsel=$(( nline * prop / 100 ))
 echo $nline $nsel
 head -n $nline data.$i |sort -R |head -n $nsel |awk '{print $1}' >>list.tmp
done

rm data.*

sort -R list.tmp |head -n $nmax > list.gen

rm list.tmp

#missing dams

awk 'NR>1 {print $1,$2,$3}' $datafile |sort -R | awk '{if ($3 == 0) $3=164500 + 1; if ($2 == 0) $2=164500 + 1 ;if (NR <25000 && $4 <5) $3=164500 + 1; if (NR <25000 && $4 >4 && $4<8) $3=164500 + 2; if (NR <25000 && $4 >7 && $4<11) $3=164500 + 3; print $0}' |sort -k1,1n >ped.dat

awk '{if ($2>164500) $2=0; if ($3>164500) $3=0; print $0}' ped.dat >ped_nogg.dat

#phenotypes
awk 'NR >1{if ($4 == "F" && $5<10) print $10}' $datafile |sed 's/+//g' >pheno.phen
awk 'NR >1{if ($4 == "F" && $5<10) print $1,1}' $datafile |sed 's/+/ /g' >tmp
awk 'NR==FNR {a[$1];next} {$3=-9; if ($1 in a) $3=$1; print $0,-9}' list.gen tmp > pheno.eff
rm tmp

#genotypes

#paste -d' ' <(awk 'NR==FNR {a[$1];next} $1 in a {printf("%20s \n", $1)}' list.gen $genofile) <(awk 'NR==FNR {a[$1];next} $1 in a {print $2}' list.gen $genofile |sed 's/3/1/g' |sed 's/4/1/g' ) >geno.dat

awk 'NR==FNR {a[$1];next} $1 in a {print $1}' list.gen $genofile > geno.id
awk 'NR==FNR {a[$1];next} $1 in a {print $2}' list.gen $genofile |sed 's/3/1/g' |sed 's/4/1/g' | sed 's/./& /g' |cut -d' ' -f1-$nsnp |sed 's/\s*$//g'  >geno.dat


#
paste -d' ' geno.id geno.dat > genoall.dat
paste -d' ' pheno.phen pheno.eff > phenoall.dat
