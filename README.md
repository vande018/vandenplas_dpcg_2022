Title of the dataset:
Data from: Deflation techniques applied on mixed model equations

Creators:
Buu-Van Nguyen      Faculty of Electrical Engineering, Mathematics and Computer Science, Delft University of Technology
Jeremie Vandenplas  Animal Breeding and Genomics, Wageningen University \& Research, ORCID: 0000-0002-2554-072X

Related publication:
Vandenplas, J., B. Nguyen, C. Vuik. 2022. Deflation techniques applied on mixed model equations. __Submitted__.

Description:
This archive contains:
 * Simulated data mimicking a dairy cattle breeding program, and based on Bradford et al. 2019 (https://doi.org/10.3168/jds.2018-15434). Simulated
   data include phenotypes of cows, pedigree of all animals, and Single Polymorphism Nucleotide genotypes for a part of the population;
 * Julia scripts for performing all analyses related to the deflated preconditioned conjugate gradient method.

This archive contains all datasets and scripts to reproduce the results of our study.
In this paper, we consider various deflation techniques applied in the Deflated
Preconditioned Conjugate Gradient (DPCG) method for solving a sparse system of linear
equations derived from a statistical linear mixed model that analyses simultaneously
phenotypic and pedigree information of genotyped and ungenotyped animals with Single
Polymorphism Nucleotide genotypes of genotyped animals.

The system of equations used in this study is the system proposed by Liu et al. 2014(https://doi.org/10.3168/jds.2014-7924).

Keywords:
dairy cattle
Julia
simulation
deflation
preconditioned conjugate gradient

This dataset contains the following files:

=====================================================================================
Folder/file                        Description
=====================================================================================
.
├── analysis
│   ├── graph_conv_1.g             # Gnuplot script for generating convergence plots from CSV files
│   ├── main.jl                    # Julia script for performing all (D)PCG analyses
│   ├── Makefile                   # Rules for performing the analyses and generating the results
│   ├── make_table.sh              # Bash script for generating tables
│   ├── name_tmp.tex               # File containing headers for the tables
│   ├── param                      # Instruction file (see description in README.md) for the Julia script `main.jl`
│   ├── README.md
│   └── script_slurm.sh            # SLURM script
├── data
│   ├── geno.dat                   # SNP genotypes (0/1/2/missing)
│   ├── geno.id                    # IDs of the animals corresponding to the SNP genotypes stored in genot.dat
│   ├── Makefile                   # Rules for generating the data from the QMSim16 files
│   ├── ped_nogg.dat               # Pedigree
│   ├── pheno.eff                  # Levels for the different effects associated with the phenotypes (in the same order as pheno.phen)
│   ├── pheno.phen                 # Phenotypes (in the same order as pheno.eff)
│   └── prepa.sh                   # Script for generating the data files from the QMSim16 files
├── julia_blup_lib
│   ├── modblup.jl                 # Julia script to set up different systems of linear equations
│   ├── modgeneral.jl              # Julia script with various common functions
│   ├── modpedigree.jl             # Julia script with functions related to the pedigree
│   ├── modpreconditioner.jl       # Julia script with functions related to the block Jacobi preconditioner
│   └── modsolver.jl               # Julia script implementing various (deflated) preconditioned conjugate gradient methods
├── Makefile                       # Rules for performing the same analyses as in the paper (rule: `all`) or new analyses (rule: `newall`)
├── README.md                      # This file
└── simulation
    ├── Makefile                   # Rules for generating the QMSim16 simulations
    ├── QMSim16                    # Executable
    ├── qmsim_bradford_2018.dat    # Instruction file for QMSim16
    └── seed.paper                 # Seed for QMSim16


Software:
* Simulation of the datasets: `QMSim16` (Linux executable);
* All `.sh` files are Bash files;
* All Julia scripts were run with `Julia 1.7.3`;
* Generation of the convergence plots: `gnuplot 5.2`.

All analyses can be run with the command in the root directory:
```shell
make
```

This dataset and scripts are published under the CC BY (Attribution) license.
This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator.

